#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 22-March-2019

# This script masters the insertion of the RD53 component data.
# it requires the progressbar library: https://pypi.org/project/progressbar2/

import os,sys,fnmatch

from AnsiColor   import Fore, Back, Style
from Exceptions  import *
from DataReader  import *
from Utils       import *
from optparse    import OptionParser
from Hybrid      import *
from rhapi       import *
from progressbar import *



if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] <path to FEH data>, ...", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the feh test outputs.')

   p.add_option( '--id',
                  action  = 'append',
                  type    = 'string',
                  default = [],
                  dest    = 'ids',
                  metavar = 'STR',
                  help    = 'ID of the FEH to be entered in database')

   p.add_option( '--verbose',
                  action  = 'store_true',
                  default = False,
                  dest    = 'verbose',
                  help    = 'Force the uploaders to print their configuration and data')

   (opt, args) = p.parse_args()

   if len(args)<1:
      p.error('need at least 1 argument!')

   if opt.verbose:  BaseUploader.verbose = True

   for p in args: hybrid_files = search_files(p,'*csv')

   if opt.verbose:  print ("hibrid files={}".format(hybrid_files))

   hybrid_conf = {}

   cbc_conf = {
      'kind_of_part'    : 'CBC3 Readout Chip',
   }

   feh_conf = {
      'kind_of_part'    : '8CBC3 Front-end Hybrid'
   }
 
   # Create the Rest Hub application for querying the database
   DBapi = RhApi('https://cmsdca.cern.ch/trk_rhapi', debug = False, sso = 'login')

   # Getting the number of Front End Hybrid already in database
   print ('Front End Hybrids components already in database: ')
   Qres  = DBapi.json2('select p.serial_number from trker_cmsr.parts p where p.kind_of_part_id=5000')
   already_in_db = [  d['serialNumber'] for d in Qres['data'] ]
   nFEHinDB = len(already_in_db)
   print ('{}'.format( nFEHinDB ) )


   # Retrieve the FE Hybrid not associated to CBC
   bar = ProgressBar(max_value=nFEHinDB, widgets=['Checking FEH association to CBC: ', \
                                                  Bar('=', '[', ']'), ' ', Percentage()] )
   feh_missing_cbc = []
   for i,feh in enumerate(already_in_db):
      query = \
      "select * from trker_cmsr.trkr_relationships_v r where r.parent_serial_number='{}'".format(feh)
      result = DBapi.json2(query)
      if len( result['data'] )==0:
         feh_missing_cbc.append(feh)
      bar.update(i)

   Uploaded = []
   for f in hybrid_files:
      print ('Processing ... {}'.format(f))
      cbc3_tag    = os.path.basename(f).split('_')[0]   

      cbc3_data   = TableReader(f,csv_delimiter=';').getDataAsCWiseDict()
      hybrid_locations    = 'CERN' #getColumnData(data,"Location")
      hybrid_serials      = getColumnData(cbc3_data,"Hybrid ID")
      hybrid_type         = getColumnData(cbc3_data,"Hybrid type")
      hybrid_manufacturer = getColumnData(cbc3_data,"MFR")
      hybrid_assembly     = getColumnData(cbc3_data,"Assembly state")
      hybrid_status       = getColumnData(cbc3_data,"Good/Bad")
      test_results        = getColumnData(cbc3_data,"Test results")
      comments            = getColumnData(cbc3_data,"Comments")

      for i,serial in enumerate(hybrid_serials):
         FEHs        = []
         # skip if serial format is not correct
         try:
            if serial=='' or len(serial.split('-')[1])!=9:  continue
         except:
            print('skip {} because of bad format'.format(serial))
            continue

         # skip serial not in the option list if option list has been provided
         if len(opt.ids)!=0:
            if serial not in options.ids:   
               if opt.verbose:
                 print('skip {} because it is not included in option'.format(serial))
               continue

         # skip serial not in the option list if it is already in DB
         if serial in already_in_db:   
            if opt.verbose:
              print('skip {} because it is already in DB'.format(serial))
            continue

         # skip serial iof BARE hybrid
         if hybrid_type[i] == 'BARE HYBRID':
            if opt.verbose:
              print('skip {} because it is a BARE Hybrid'.format(serial))
            continue

         #extract main configuration parameters
         feh_state = hybrid_status[i].strip() if hybrid_status[i]!='' else 'Good'
         chip_type = hybrid_type[i].split(' ')[0][1:]
         feh_space = '1.8 mm'
         stif_type = 'FR4' if 'FR4 stiffener' in hybrid_type[i] else 'CF-K13D2U'
         ass_state = hybrid_assembly[i].capitalize().strip()

         # format comment description
         c_txt = ''
         if test_results[i]!='' or comments[i]!='':
             import string
             RE = ''.join([j if ord(j)<128 else ' ' for j in test_results[i]])\
                  .lstrip()
             CO = ''.join([j if ord(j)<128 else ' ' for j in comments[i]])\
                  .lstrip()
             CO = CO.capitalize() if CO!='' and not CO[0].isupper() else CO
             c_txt =  RE if CO !='' else ''
             c_txt += '. ' if RE !='' and CO !='' and \
                                      RE[-1] not in string.punctuation else ' '
             c_txt += CO if CO !='' else ''
             c_txt += '.' if c_txt[-1] not in string.punctuation else ''
             #str = ''.join([i if ord(i) < 128 else ' ' for i in str])
         
         # setup configuration dictionary
         feh_conf['manufacturer'] = hybrid_manufacturer[i]
         feh_conf['unique_location'] = hybrid_locations
         #feh_conf = {
         #   'kind_of_part':    '8CBC3 Front-end Hybrid',
         #   'manufacturer':    hybrid_manufacturer[i],
         #   'unique_location': hybrid_locations,
         #}

         feh = Upload8CBC3(serial, feh_conf, feh_state, chip_type, feh_space,\
                           stif_type, ass_state, c_txt)

         feh.dump_xml_data(uploaded=FEHs, excluded=already_in_db)
         Uploaded.extend(FEHs)


   to_be_associated = list(set(Uploaded+feh_missing_cbc))

   Associated = []
   for serial in to_be_associated:
      with_cbc = []
      associate = AttachCBC2FEH(serial,feh_conf['kind_of_part'],opt.data_path,\
                                8,cbc_conf)
      try:
         associate.dump_xml_data(associated=with_cbc)
         Associated.extend( with_cbc )
      except:
         continue

   print ('{} FE Hybrids uploaded:'.format( len(Uploaded) ))
   for h in Uploaded:
      print ('{}'.format(h))

   notAssociated = [s for s in to_be_associated if s not in Associated]
   NotAssociated = list( set(notAssociated)  )
   print ('\n{} FE Hybrids not associated:'.format( len(NotAssociated) ))
   for h in NotAssociated:
      print ('{} not associated'.format(h))

   #Zip upload files
   from zipfile import ZipFile
   feh_files = search_files('.','*_FEHybrids.xml')
   with ZipFile('FEH.zip', 'w') as (zip):
      for f in feh_files: zip.write(f)
   for f in feh_files:  os.remove(f)


   ass_files = search_files('.','*_chip_association.xml')
   with ZipFile('FEH_cbc.zip', 'w') as (zip):
      for f in ass_files: zip.write(f)
   for f in ass_files:  os.remove(f)
