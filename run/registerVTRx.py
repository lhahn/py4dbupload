#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 01-January-2023

# This script masters the insertion of the CBC component data.

#import os,sys,fnmatch

#from AnsiColor  import Fore, Back, Style
#from Exceptions import *
from Utils        import search_files,DBupload,UploaderContainer
from DataReader   import TableReader
from BaseUploader import BaseUploader
from VTRx         import VTRx
from datetime     import date
from dateutil     import parser
from optparse     import OptionParser
from progressbar  import *

import os,time



if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] <vtrx file>  |  %prog [options (directory with  vtrx files)]", version="1.1")

   p.add_option( '--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the csv tables with VTRx+ data.')
   
   p.add_option( '-v','--ver',
               type    = 'string',
               default = '1.0',
               dest    = 'version',
               metavar = 'STR',
               help    = 'Version type of the components.')
   
   p.add_option( '-d','--desc',
               type    = 'string',
               default = None,
               dest    = 'description',
               metavar = 'STR',
               help    = 'Input a description for the data to be uploaded.')
   
   p.add_option( '-i','--inserter',
               type    = 'string',
               default = None,
               dest    = 'inserter',
               metavar = 'STR',
               help    = 'Overwirtes the account name put in the RECORD_INSERTION_USER column.')
   
   p.add_option( '--date',
               type    = 'string',
               default = '',
               dest    = 'date',
               metavar = 'STR',
               help    = 'Date of the component production.')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   
   p.add_option( '--update',
               action  = 'store_true',
               default = False,
               dest    = 'update',
               help    = 'Force the upload of components in update mode')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database.')
   
   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data.')

   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data.')

   (opt, args) = p.parse_args()

   if len(args)>1:
      p.error('accepts at most 1 argument!')

   upload_mode = 'update' if opt.update else 'insert'

   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose  = opt.verbose
   BaseUploader.debug    = opt.debug
   
   date_of_production = date.strftime(parser.parse(opt.date),'%Y-%m-%d') \
                            if opt.date!='' else None
   

   vtrx_files=[]
   if len(args)==1:  vtrx_files.append( args[0] )
   else:             vtrx_files = sorted( search_files(opt.data_path,'*.csv') )

   # VTRx default configuration
   vtrx_conf = {
      'kind_of_part'    : 'VTRx+',
      'unique_location' : 'AEMtec',
      'product_date'    : date_of_production,
      'attributes'      : [('Status','Good')]  # Default Status for Wafer is Bad
   }
   
   if opt.inserter!=''   : vtrx_conf['inserter']=opt.inserter
   if opt.description!='': vtrx_conf['description']=opt.description
   if opt.version!=''    : vtrx_conf['version']=opt.version

   
   files_to_be_uploaded = []
   VTRx_components = UploaderContainer('VTRx')

   for file in vtrx_files:
      vtrx_data = TableReader(file,csv_delimiter=',',tabSize=14)
      print ('\n\nWafer to upload',vtrx_data)
      data = vtrx_data.getDataAsCWiseDictRowSplit()

      nVTRx = len(data)
      bar = progressbar(range(nVTRx), widgets=[f'Processing {vtrx_data.filename}: ', \
             Bar('=', '[', ']'), ' ', Percentage()])
   
      for i in bar:
         w = data[i]
         vtrx = VTRx(vtrx_conf,w['idDevice_sort'],w['idDriver_sort'],w['codePigtail_sort'])
         time.sleep(0.1)
         VTRx_components.add(vtrx)
   
   files_to_be_uploaded.append(VTRx_components.dump_xml_data())
   
   # Upload files in the database 
   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path ,verbose=True)
   if opt.upload:  
      for fi in files_to_be_uploaded:
         # Files upload
         db_loader.upload_data(fi)