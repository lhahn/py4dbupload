07.06.2019 17:39 ||I| 


********************************************************************************
                                        [1m[31mHW SUMMARY: [0m
********************************************************************************

[1m[36m|----BeBoard  Id :[1m[34m0[1m[36m BoardType: [1m[34mD19C[1m[36m EventType: [1m[31mVR[0m
[1m[34m|       |----Board Id:      [1m[33mboard
[1m[34m|       |----URI:           [1m[33mchtcp-2.0://localhost:10203?target=192.168.0.10:50001
[1m[34m|       |----Address Table: [1m[33mfile://settings/address_tables/d19c_address_table.xml
[1m[34m|       |[0m
[34m|	|----Register  clock_source: [1m[33m3[0m
[34m|	|----Register  fc7_daq_cnfg.clock.ext_clk_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.ttc.ttc_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.triggers_to_accept: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.trigger_source: [1m[33m7[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.user_trigger_frequency: [1m[33m100[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stubs_mask: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_delay_value: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_veto_length: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_fast_reset: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_test_pulse: [1m[33m20[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_before_next_pulse: [1m[33m700[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_fast_reset: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_test_pulse: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_l1a: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.ext_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.antenna_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.delay_between_two_consecutive: [1m[33m10[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.backpressure_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.stubOR: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.initial_fast_reset_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.physical_interface_block.i2c.frequency: [1m[33m4[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.packet_nbr: [1m[33m99[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_handshake_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_rate: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.trigger_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.common_stubdata_delay: [1m[33m5[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.dio5_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.handshake_mode: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.tlu_enabled: [1m[33m0[0m
[34m|	|[0m
[1m[36m|	|----Module  FeId :0[0m
[32m|	|	|----CBC Files Path : hybrid_functional_tests/8CBC318-201000074/calibration/Calibration_Electron_07-06-19_17h34/[0m
[1m[36m|	|	|----CBC  Id :0, File: FE0CBC0.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :1, File: FE0CBC1.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :2, File: FE0CBC2.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :3, File: FE0CBC3.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :4, File: FE0CBC4.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :5, File: FE0CBC5.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :6, File: FE0CBC6.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :7, File: FE0CBC7.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----Global CBC Settings: [0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[32m|	|	|	|----VCth: [31m0x23a (570)[0m
[32m|	|	|	|----TriggerLatency: [31m0x13 (19)[0m
[32m|	|	|	|----TestPulse: enabled: [31m0[32m, polarity: [31m0[32m, amplitude: [31m128[32m (0x80)[0m
[32m|	|	|	|               channelgroup: [31m0[32m, delay: [31m0[32m, groundohters: [31m1[0m
[32m|	|	|	|----Cluster & Stub Logic: ClusterWidthDiscrimination: [31m4[32m, PtWidth: [31m14[32m, Layerswap: [31m0[0m
[32m|	|	|	|                          Offset1: [31m0[32m, Offset2: [31m0[32m, Offset3: [31m0[32m, Offset4: [31m0[0m
[32m|	|	|	|----Misc Settings:  PipelineLogicSource: [31m0[32m, StubLogicSource: [31m1[32m, OR254: [31m0[32m, TPG Clock: [31m1[32m, Test Clock 40: [31m1[32m, DLL: [31m21[0m
[32m|	|	|	|----Analog Mux value: [31m0 (0x0, 0b00000)[0m
[32m|	|	|	|----List of disabled Channels: [0m
[34m|	|
|	|----SLink[0m
[34m|	|       |----DebugMode[35m : SLinkDebugMode::FULL[0m
[34m|	|       |----ConditionData: Type [31mI2C VCth1[34m, UID [31m1[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m4f[34m, Value [35m58[0m
[34m|	|       |----ConditionData: Type [31mUser [34m, UID [31m128[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m34[0m
[34m|	|       |----ConditionData: Type [31mHV [34m, UID [31m5[34m, FeId [31m0[34m, CbcId [31m2[34m, Page [31m0[34m, Address [31m0[34m, Value [35m250[0m
[34m|	|       |----ConditionData: Type [31mTDC [34m, UID [31m3[34m, FeId [31m255[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m0[0m


********************************************************************************
                                        [1m[31mEND OF HW SUMMARY: [0m
********************************************************************************


[31mSetting[0m --[1m[36mTargetVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mTargetOffset[0m:[1m[33m80[0m
[31mSetting[0m --[1m[36mNevents[0m:[1m[33m100[0m
[31mSetting[0m --[1m[36mTestPulsePotentiometer[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mHoleMode[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mVerificationLoop[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mInitialVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mSignalScanStep[0m:[1m[33m2[0m
[31mSetting[0m --[1m[36mFitSignal[0m:[1m[33m0[0m

07.06.2019 17:39 ||I| Creating directory: Results/IntegratedTester_Electron_07-06-19_17h39
Info in <TCivetweb::Create>: Starting HTTP server on port 8080
07.06.2019 17:39 ||I| Opening THttpServer on port 8080. Point your browser to: [1m[32mcmsuptkhsetup1.dyndns.cern.ch:8080[0m
07.06.2019 17:39 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
07.06.2019 17:39 ||I| [1m[32mSetting the I2C address table[0m
07.06.2019 17:39 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
07.06.2019 17:39 ||I| Enabling Hybrid 0
07.06.2019 17:39 ||I|      Enabling Chip 0
07.06.2019 17:39 ||I|      Enabling Chip 1
07.06.2019 17:39 ||I|      Enabling Chip 2
07.06.2019 17:39 ||I|      Enabling Chip 3
07.06.2019 17:39 ||I|      Enabling Chip 4
07.06.2019 17:39 ||I|      Enabling Chip 5
07.06.2019 17:39 ||I|      Enabling Chip 6
07.06.2019 17:39 ||I|      Enabling Chip 7
07.06.2019 17:39 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 2000007c
Reply from chip 1: 2004007c
Reply from chip 2: 2008007c
Reply from chip 3: 200c007c
Reply from chip 4: 2010007c
Reply from chip 5: 2014007c
Reply from chip 6: 2018007c
Reply from chip 7: 201c007c
07.06.2019 17:39 ||I| Successfully received *Pings* from 8 Cbcs
07.06.2019 17:39 ||I| [32mCBC3 Phase tuning finished succesfully[0m
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| Waiting for DDR3 to finish initial calibration
07.06.2019 17:39 ||I| [32mSuccessfully configured Board 0[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 0[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 1[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 2[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 3[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 4[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 5[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 6[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 7[0m
07.06.2019 17:39 ||I| [32mCalibrating CBCs before starting antenna test of the CBCs on the DUT.[0m
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 17:39 ||I| [1m[34m CHIP ID FUSE 398913[0m
07.06.2019 17:39 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 17:39 ||I| [1m[34m CHIP ID FUSE 398928[0m
07.06.2019 17:39 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 17:39 ||I| [1m[34m CHIP ID FUSE 398943[0m
07.06.2019 17:39 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 17:39 ||I| [1m[34m CHIP ID FUSE 398956[0m
07.06.2019 17:39 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 17:39 ||I| [1m[34m CHIP ID FUSE 398969[0m
07.06.2019 17:39 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 17:39 ||I| [1m[34m CHIP ID FUSE 398980[0m
07.06.2019 17:39 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 17:39 ||I| [1m[34m CHIP ID FUSE 398990[0m
07.06.2019 17:39 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
07.06.2019 17:39 ||I| [1m[34m CHIP ID FUSE 398998[0m
07.06.2019 17:39 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| Created Object Maps and parsed settings:
07.06.2019 17:39 ||I| 	Nevents = 100
07.06.2019 17:39 ||I| 	TestPulseAmplitude = 0
07.06.2019 17:39 ||I|   Target Vcth determined algorithmically for CBC3
07.06.2019 17:39 ||I|   Target Offset fixed to half range (0x80) for CBC3
07.06.2019 17:39 ||I| [1m[34mExtracting Target VCth ...[0m
07.06.2019 17:39 ||I| Disabling all channels by setting offsets to 0xff
07.06.2019 17:39 ||I| Setting offsets of Test Group -1 to 0xff
07.06.2019 17:39 ||I| [32mEnabling Test Group....-1[0m
07.06.2019 17:39 ||I| Setting offsets of Test Group -1 to 0x80
07.06.2019 17:39 ||I| [31mDisabling Test Group....-1[0m
07.06.2019 17:39 ||I| Setting offsets of Test Group -1 to 0xff
07.06.2019 17:39 ||I| [1m[32mMean VCth value for FE 0 CBC 0 is [1m[31m597[0m
07.06.2019 17:39 ||I| [1m[32mMean VCth value for FE 0 CBC 1 is [1m[31m599[0m
07.06.2019 17:39 ||I| [1m[32mMean VCth value for FE 0 CBC 2 is [1m[31m589[0m
07.06.2019 17:39 ||I| [1m[32mMean VCth value for FE 0 CBC 3 is [1m[31m591[0m
07.06.2019 17:39 ||I| [1m[32mMean VCth value for FE 0 CBC 4 is [1m[31m593[0m
07.06.2019 17:39 ||I| [1m[32mMean VCth value for FE 0 CBC 5 is [1m[31m597[0m
07.06.2019 17:39 ||I| [1m[32mMean VCth value for FE 0 CBC 6 is [1m[31m599[0m
07.06.2019 17:39 ||I| [1m[32mMean VCth value for FE 0 CBC 7 is [1m[31m585[0m
07.06.2019 17:39 ||I| [1m[34mMean VCth value of all chips is 593 - using as TargetVcth value for all chips![0m
07.06.2019 17:39 ||I| [32mEnabling Test Group....0[0m
07.06.2019 17:39 ||I| Setting offsets of Test Group 0 to 0xff
07.06.2019 17:39 ||I| [31mDisabling Test Group....0[0m
07.06.2019 17:39 ||I| [32mEnabling Test Group....1[0m
07.06.2019 17:39 ||I| Setting offsets of Test Group 1 to 0xff
07.06.2019 17:39 ||I| [31mDisabling Test Group....1[0m
07.06.2019 17:39 ||I| [32mEnabling Test Group....2[0m
07.06.2019 17:39 ||I| Setting offsets of Test Group 2 to 0xff
07.06.2019 17:39 ||I| [31mDisabling Test Group....2[0m
07.06.2019 17:39 ||I| [32mEnabling Test Group....3[0m
07.06.2019 17:39 ||I| Setting offsets of Test Group 3 to 0xff
07.06.2019 17:39 ||I| [31mDisabling Test Group....3[0m
07.06.2019 17:39 ||I| [32mEnabling Test Group....4[0m
07.06.2019 17:39 ||I| Setting offsets of Test Group 4 to 0xff
07.06.2019 17:39 ||I| [31mDisabling Test Group....4[0m
07.06.2019 17:39 ||I| [32mEnabling Test Group....5[0m
07.06.2019 17:39 ||I| Setting offsets of Test Group 5 to 0xff
07.06.2019 17:39 ||I| [31mDisabling Test Group....5[0m
07.06.2019 17:39 ||I| [32mEnabling Test Group....6[0m
07.06.2019 17:39 ||I| Setting offsets of Test Group 6 to 0xff
07.06.2019 17:39 ||I| [31mDisabling Test Group....6[0m
07.06.2019 17:39 ||I| [32mEnabling Test Group....7[0m
07.06.2019 17:39 ||I| Setting offsets of Test Group 7 to 0xff
07.06.2019 17:39 ||I| [31mDisabling Test Group....7[0m
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 17:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
07.06.2019 17:39 ||I| [1m[32mApplying final offsets determined by tuning to chip - no re-configure necessary![0m
07.06.2019 17:39 ||I| Results saved!
07.06.2019 17:39 ||I| [1m[34mConfigfiles for all Cbcs written to Results/IntegratedTester_Electron_07-06-19_17h39[0m
07.06.2019 17:39 ||I| Calibration finished.
Calibration of the DUT finished at: Fri Jun  7 17:39:58 2019
	elapsed time: 9.92006 seconds
07.06.2019 17:39 ||I| Starting noise occupancy test.
07.06.2019 17:39 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
07.06.2019 17:39 ||I| [1m[32mSetting the I2C address table[0m
07.06.2019 17:39 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
07.06.2019 17:39 ||I| Enabling Hybrid 0
07.06.2019 17:39 ||I|      Enabling Chip 0
07.06.2019 17:39 ||I|      Enabling Chip 1
07.06.2019 17:39 ||I|      Enabling Chip 2
07.06.2019 17:39 ||I|      Enabling Chip 3
07.06.2019 17:39 ||I|      Enabling Chip 4
07.06.2019 17:39 ||I|      Enabling Chip 5
07.06.2019 17:39 ||I|      Enabling Chip 6
07.06.2019 17:39 ||I|      Enabling Chip 7
07.06.2019 17:39 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 2000007c
Reply from chip 1: 2004007c
Reply from chip 2: 2008007c
Reply from chip 3: 200c007c
Reply from chip 4: 2010007c
Reply from chip 5: 2014007c
Reply from chip 6: 2018007c
Reply from chip 7: 201c007c
07.06.2019 17:39 ||I| Successfully received *Pings* from 8 Cbcs
07.06.2019 17:39 ||I| [32mCBC3 Phase tuning finished succesfully[0m
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
07.06.2019 17:39 ||I| 		 Mode: 0
07.06.2019 17:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
07.06.2019 17:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
07.06.2019 17:39 ||I| 		 Delay: 15, Bitslip: 3
07.06.2019 17:39 ||I| Waiting for DDR3 to finish initial calibration
07.06.2019 17:39 ||I| [32mSuccessfully configured Board 0[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 0[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 1[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 2[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 3[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 4[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 5[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 6[0m
07.06.2019 17:39 ||I| [32mSuccessfully configured Cbc 7[0m
07.06.2019 17:39 ||I| Trigger source 01: 7
07.06.2019 17:39 ||I| Histo Map for Module 0 does not exist - creating 
07.06.2019 17:39 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
07.06.2019 17:39 ||I| 7
07.06.2019 17:39 ||I| Mesuring Efficiency per Strip ... 
07.06.2019 17:39 ||I| Taking data with 100 Events!
07.06.2019 17:39 ||I| 		Mean occupancy for the Top side: 61.8 error= 8.48089[0m
07.06.2019 17:39 ||I| 		Mean occupancy for the Botton side: 62.1094 error= 7.94171[0m
# Noisy channels on Bottom Sensor : 3,7,10,11,12,14,18,23,24,25,26,28,31,33,34,35,36,42,43,44,45,50,51,54,60,63,70,71,73,78,79,80,81,83,86,89,92,97,104,105,109,111,115,116,117,120,123,125,127,131,132,135,136,138,140,142,144,146,147,152,153,155,164,167,168,172,180,183,186,187,188,193,194,195,198,200,206,208,214,215,219,220,223,227,234,235,236,239,241,245,251,257,258,261,264,266,268,271,277,279,280,281,283,286,287,289,292,294,299,300,302,306,308,310,313,314,315,318,321,322,326,328,330,337,338,339,340,342,344,346,349,352,353,355,363,366,369,371,373,375,378,381,387,388,391,392,393,396,397,399,400,402,403,404,406,407,408,410,412,413,414,416,419,420,426,427,432,434,438,439,441,442,443,444,445,447,452,453,455,458,461,463,465,466,467,470,471,472,473,477,478,480,483,484,487,488,489,492,494,495,496,497,498,502,507,508,509,511,515,517,518,521,524,527,530,531,533,536,537,539,541,542,543,551,553,557,558,563,569,570,573,577,578,579,580,581,588,594,595,596,597,598,599,601,603,604,605,606,612,613,614,615,617,618,620,621,622,623,626,629,630,631,632,635,639,644,652,653,656,657,658,659,660,661,668,670,671,674,676,677,679,680,683,688,690,691,694,695,697,698,699,702,703,704,707,712,716,720,723,724,729,730,731,733,734,736,740,744,745,746,754,755,760,761,762,765,768,771,778,779,781,791,794,797,799,802,804,807,813,816,819,825,826,827,828,830,832,834,837,838,844,846,847,851,853,858,859,865,871,881,888,890,892,894,900,903,904,906,907,910,919,925,926,932,933,934,935,938,943,945,946,947,948,953,956,961,962,965,968,972,979,981,984,986,987,988,989,990,993,994,996,1000,1003,1004,1005,1008,1009,1011,1012,1014,1015
# Noisy channels on Top Sensor : 4,5,7,10,11,13,15,16,19,21,22,23,25,28,32,33,44,46,50,51,56,59,62,64,66,70,72,75,78,79,86,88,90,91,93,98,100,103,111,113,116,120,125,126,131,140,141,143,144,147,155,157,158,159,160,163,165,167,168,174,177,183,184,186,187,206,208,212,213,215,216,223,224,228,230,232,233,238,239,242,247,248,249,250,253,254,256,257,259,264,265,266,269,273,274,275,276,277,280,283,284,285,289,290,291,292,296,297,298,300,301,306,307,309,310,313,314,315,320,322,323,325,326,330,331,333,334,336,337,340,341,342,345,349,353,355,356,357,362,363,365,366,368,372,374,375,377,378,380,381,382,383,387,390,394,397,411,416,417,418,424,426,428,431,433,434,435,436,437,440,442,445,446,449,451,453,455,456,459,460,461,465,468,469,472,476,478,480,482,483,485,486,494,495,496,498,499,506,513,518,520,521,529,531,535,536,537,540,546,547,549,550,553,555,556,558,560,563,566,569,570,573,574,575,579,581,587,590,591,593,594,595,596,597,608,611,615,616,621,624,626,627,628,629,633,635,638,639,640,643,646,647,648,651,652,653,654,655,657,658,665,667,668,669,670,671,677,678,680,682,685,688,690,691,694,695,696,697,698,699,703,704,706,708,709,711,715,716,718,719,720,721,722,723,728,729,732,734,735,736,737,738,739,741,742,745,746,748,750,755,756,757,758,769,770,771,772,776,778,780,781,782,783,785,787,789,797,802,803,805,809,811,819,822,827,832,835,841,843,846,854,856,861,872,875,877,878,879,882,890,891,892,894,895,896,897,905,909,911,916,917,920,921,923,924,933,936,937,941,942,945,946,948,950,952,958,959,960,961,964,965,966,969,970,972,977,983,985,986,993,996,998,999,1001,1004,1007,1014
# Dead channels on Bottom Sensor : 
# Dead channels on Top Sensor : 
07.06.2019 17:39 ||I| Results saved!
(Noise) Occupancy measurement finished at: Fri Jun  7 17:39:59 2019
	elapsed time: 1.38374 seconds
Complete system test finished at: Fri Jun  7 17:39:59 2019
	elapsed time: 12.6551 seconds
07.06.2019 17:39 ||I| [1m[31mclosing result file![0m
07.06.2019 17:39 ||I| [1m[31mDestroying memory objects[0m
User comment: 
