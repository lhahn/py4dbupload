12.06.2019 14:39 ||I| 


********************************************************************************
                                        [1m[31mHW SUMMARY: [0m
********************************************************************************

[1m[36m|----BeBoard  Id :[1m[34m0[1m[36m BoardType: [1m[34mD19C[1m[36m EventType: [1m[31mVR[0m
[1m[34m|       |----Board Id:      [1m[33mboard
[1m[34m|       |----URI:           [1m[33mchtcp-2.0://localhost:10203?target=192.168.0.10:50001
[1m[34m|       |----Address Table: [1m[33mfile://settings/address_tables/d19c_address_table.xml
[1m[34m|       |[0m
[34m|	|----Register  clock_source: [1m[33m3[0m
[34m|	|----Register  fc7_daq_cnfg.clock.ext_clk_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.ttc.ttc_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.triggers_to_accept: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.trigger_source: [1m[33m7[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.user_trigger_frequency: [1m[33m100[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stubs_mask: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_delay_value: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_veto_length: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_fast_reset: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_test_pulse: [1m[33m20[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_before_next_pulse: [1m[33m700[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_fast_reset: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_test_pulse: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_l1a: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.ext_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.antenna_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.delay_between_two_consecutive: [1m[33m10[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.backpressure_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.stubOR: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.initial_fast_reset_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.physical_interface_block.i2c.frequency: [1m[33m4[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.packet_nbr: [1m[33m99[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_handshake_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_rate: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.trigger_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.common_stubdata_delay: [1m[33m5[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.dio5_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.handshake_mode: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.tlu_enabled: [1m[33m0[0m
[34m|	|[0m
[1m[36m|	|----Module  FeId :0[0m
[32m|	|	|----CBC Files Path : hybrid_functional_tests/8CBC318-201000074/calibration/Calibration_Electron_12-06-19_14h34/[0m
[1m[36m|	|	|----CBC  Id :0, File: FE0CBC0.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :1, File: FE0CBC1.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :2, File: FE0CBC2.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :3, File: FE0CBC3.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :4, File: FE0CBC4.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :5, File: FE0CBC5.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :6, File: FE0CBC6.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :7, File: FE0CBC7.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----Global CBC Settings: [0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[32m|	|	|	|----VCth: [31m0x23a (570)[0m
[32m|	|	|	|----TriggerLatency: [31m0x13 (19)[0m
[32m|	|	|	|----TestPulse: enabled: [31m0[32m, polarity: [31m0[32m, amplitude: [31m128[32m (0x80)[0m
[32m|	|	|	|               channelgroup: [31m0[32m, delay: [31m0[32m, groundohters: [31m1[0m
[32m|	|	|	|----Cluster & Stub Logic: ClusterWidthDiscrimination: [31m4[32m, PtWidth: [31m14[32m, Layerswap: [31m0[0m
[32m|	|	|	|                          Offset1: [31m0[32m, Offset2: [31m0[32m, Offset3: [31m0[32m, Offset4: [31m0[0m
[32m|	|	|	|----Misc Settings:  PipelineLogicSource: [31m0[32m, StubLogicSource: [31m1[32m, OR254: [31m0[32m, TPG Clock: [31m1[32m, Test Clock 40: [31m1[32m, DLL: [31m21[0m
[32m|	|	|	|----Analog Mux value: [31m0 (0x0, 0b00000)[0m
[32m|	|	|	|----List of disabled Channels: [0m
[34m|	|
|	|----SLink[0m
[34m|	|       |----DebugMode[35m : SLinkDebugMode::FULL[0m
[34m|	|       |----ConditionData: Type [31mI2C VCth1[34m, UID [31m1[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m4f[34m, Value [35m58[0m
[34m|	|       |----ConditionData: Type [31mUser [34m, UID [31m128[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m34[0m
[34m|	|       |----ConditionData: Type [31mHV [34m, UID [31m5[34m, FeId [31m0[34m, CbcId [31m2[34m, Page [31m0[34m, Address [31m0[34m, Value [35m250[0m
[34m|	|       |----ConditionData: Type [31mTDC [34m, UID [31m3[34m, FeId [31m255[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m0[0m


********************************************************************************
                                        [1m[31mEND OF HW SUMMARY: [0m
********************************************************************************


[31mSetting[0m --[1m[36mTargetVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mTargetOffset[0m:[1m[33m80[0m
[31mSetting[0m --[1m[36mNevents[0m:[1m[33m100[0m
[31mSetting[0m --[1m[36mTestPulsePotentiometer[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mHoleMode[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mVerificationLoop[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mInitialVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mSignalScanStep[0m:[1m[33m2[0m
[31mSetting[0m --[1m[36mFitSignal[0m:[1m[33m0[0m

12.06.2019 14:39 ||I| Creating directory: Results/IntegratedTester_Electron_12-06-19_14h39
Info in <TCivetweb::Create>: Starting HTTP server on port 8080
12.06.2019 14:39 ||I| Opening THttpServer on port 8080. Point your browser to: [1m[32mcmsuptkhsetup1.dyndns.cern.ch:8080[0m
12.06.2019 14:39 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
12.06.2019 14:39 ||I| [1m[32mSetting the I2C address table[0m
12.06.2019 14:39 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
12.06.2019 14:39 ||I| Enabling Hybrid 0
12.06.2019 14:39 ||I|      Enabling Chip 0
12.06.2019 14:39 ||I|      Enabling Chip 1
12.06.2019 14:39 ||I|      Enabling Chip 2
12.06.2019 14:39 ||I|      Enabling Chip 3
12.06.2019 14:39 ||I|      Enabling Chip 4
12.06.2019 14:39 ||I|      Enabling Chip 5
12.06.2019 14:39 ||I|      Enabling Chip 6
12.06.2019 14:39 ||I|      Enabling Chip 7
12.06.2019 14:39 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 2000007c
Reply from chip 1: 2004007c
Reply from chip 2: 2008007c
Reply from chip 3: 200c007c
Reply from chip 4: 2010007c
Reply from chip 5: 2014007c
Reply from chip 6: 2018007c
Reply from chip 7: 201c007c
12.06.2019 14:39 ||I| Successfully received *Pings* from 8 Cbcs
12.06.2019 14:39 ||I| [32mCBC3 Phase tuning finished succesfully[0m
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| Waiting for DDR3 to finish initial calibration
12.06.2019 14:39 ||I| [32mSuccessfully configured Board 0[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 0[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 1[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 2[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 3[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 4[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 5[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 6[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 7[0m
12.06.2019 14:39 ||I| [32mCalibrating CBCs before starting antenna test of the CBCs on the DUT.[0m
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 14:39 ||I| [1m[34m CHIP ID FUSE 5696[0m
12.06.2019 14:39 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 14:39 ||I| [1m[34m CHIP ID FUSE 5681[0m
12.06.2019 14:39 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 14:39 ||I| [1m[34m CHIP ID FUSE 5666[0m
12.06.2019 14:39 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 14:39 ||I| [1m[34m CHIP ID FUSE 5652[0m
12.06.2019 14:39 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 14:39 ||I| [1m[34m CHIP ID FUSE 5638[0m
12.06.2019 14:39 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 14:39 ||I| [1m[34m CHIP ID FUSE 5626[0m
12.06.2019 14:39 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 14:39 ||I| [1m[34m CHIP ID FUSE 5615[0m
12.06.2019 14:39 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
12.06.2019 14:39 ||I| [1m[34m CHIP ID FUSE 5606[0m
12.06.2019 14:39 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| Created Object Maps and parsed settings:
12.06.2019 14:39 ||I| 	Nevents = 100
12.06.2019 14:39 ||I| 	TestPulseAmplitude = 0
12.06.2019 14:39 ||I|   Target Vcth determined algorithmically for CBC3
12.06.2019 14:39 ||I|   Target Offset fixed to half range (0x80) for CBC3
12.06.2019 14:39 ||I| [1m[34mExtracting Target VCth ...[0m
12.06.2019 14:39 ||I| Disabling all channels by setting offsets to 0xff
12.06.2019 14:39 ||I| Setting offsets of Test Group -1 to 0xff
12.06.2019 14:39 ||I| [32mEnabling Test Group....-1[0m
12.06.2019 14:39 ||I| Setting offsets of Test Group -1 to 0x80
12.06.2019 14:39 ||I| [31mDisabling Test Group....-1[0m
12.06.2019 14:39 ||I| Setting offsets of Test Group -1 to 0xff
12.06.2019 14:39 ||I| [1m[32mMean VCth value for FE 0 CBC 0 is [1m[31m592[0m
12.06.2019 14:39 ||I| [1m[32mMean VCth value for FE 0 CBC 1 is [1m[31m585[0m
12.06.2019 14:39 ||I| [1m[32mMean VCth value for FE 0 CBC 2 is [1m[31m589[0m
12.06.2019 14:39 ||I| [1m[32mMean VCth value for FE 0 CBC 3 is [1m[31m608[0m
12.06.2019 14:39 ||I| [1m[32mMean VCth value for FE 0 CBC 4 is [1m[31m604[0m
12.06.2019 14:39 ||I| [1m[32mMean VCth value for FE 0 CBC 5 is [1m[31m578[0m
12.06.2019 14:39 ||I| [1m[32mMean VCth value for FE 0 CBC 6 is [1m[31m582[0m
12.06.2019 14:39 ||I| [1m[32mMean VCth value for FE 0 CBC 7 is [1m[31m450[0m
12.06.2019 14:39 ||I| [1m[34mMean VCth value of all chips is 573 - using as TargetVcth value for all chips![0m
12.06.2019 14:39 ||I| [32mEnabling Test Group....0[0m
12.06.2019 14:39 ||I| Setting offsets of Test Group 0 to 0xff
12.06.2019 14:39 ||I| [31mDisabling Test Group....0[0m
12.06.2019 14:39 ||I| [32mEnabling Test Group....1[0m
12.06.2019 14:39 ||I| Setting offsets of Test Group 1 to 0xff
12.06.2019 14:39 ||I| [31mDisabling Test Group....1[0m
12.06.2019 14:39 ||I| [32mEnabling Test Group....2[0m
12.06.2019 14:39 ||I| Setting offsets of Test Group 2 to 0xff
12.06.2019 14:39 ||I| [31mDisabling Test Group....2[0m
12.06.2019 14:39 ||I| [32mEnabling Test Group....3[0m
12.06.2019 14:39 ||I| Setting offsets of Test Group 3 to 0xff
12.06.2019 14:39 ||I| [31mDisabling Test Group....3[0m
12.06.2019 14:39 ||I| [32mEnabling Test Group....4[0m
12.06.2019 14:39 ||I| Setting offsets of Test Group 4 to 0xff
12.06.2019 14:39 ||I| [31mDisabling Test Group....4[0m
12.06.2019 14:39 ||I| [32mEnabling Test Group....5[0m
12.06.2019 14:39 ||I| Setting offsets of Test Group 5 to 0xff
12.06.2019 14:39 ||I| [31mDisabling Test Group....5[0m
12.06.2019 14:39 ||I| [32mEnabling Test Group....6[0m
12.06.2019 14:39 ||I| Setting offsets of Test Group 6 to 0xff
12.06.2019 14:39 ||I| [31mDisabling Test Group....6[0m
12.06.2019 14:39 ||I| [32mEnabling Test Group....7[0m
12.06.2019 14:39 ||I| Setting offsets of Test Group 7 to 0xff
12.06.2019 14:39 ||I| [31mDisabling Test Group....7[0m
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 14:39 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
12.06.2019 14:39 ||I| [1m[32mApplying final offsets determined by tuning to chip - no re-configure necessary![0m
12.06.2019 14:39 ||I| Results saved!
12.06.2019 14:39 ||I| [1m[34mConfigfiles for all Cbcs written to Results/IntegratedTester_Electron_12-06-19_14h39[0m
12.06.2019 14:39 ||I| Calibration finished.
Calibration of the DUT finished at: Wed Jun 12 14:39:40 2019
	elapsed time: 9.95313 seconds
12.06.2019 14:39 ||I| Starting noise occupancy test.
12.06.2019 14:39 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
12.06.2019 14:39 ||I| [1m[32mSetting the I2C address table[0m
12.06.2019 14:39 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
12.06.2019 14:39 ||I| Enabling Hybrid 0
12.06.2019 14:39 ||I|      Enabling Chip 0
12.06.2019 14:39 ||I|      Enabling Chip 1
12.06.2019 14:39 ||I|      Enabling Chip 2
12.06.2019 14:39 ||I|      Enabling Chip 3
12.06.2019 14:39 ||I|      Enabling Chip 4
12.06.2019 14:39 ||I|      Enabling Chip 5
12.06.2019 14:39 ||I|      Enabling Chip 6
12.06.2019 14:39 ||I|      Enabling Chip 7
12.06.2019 14:39 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 2000007c
Reply from chip 1: 2004007c
Reply from chip 2: 2008007c
Reply from chip 3: 200c007c
Reply from chip 4: 2010007c
Reply from chip 5: 2014007c
Reply from chip 6: 2018007c
Reply from chip 7: 201c007c
12.06.2019 14:39 ||I| Successfully received *Pings* from 8 Cbcs
12.06.2019 14:39 ||I| [32mCBC3 Phase tuning finished succesfully[0m
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
12.06.2019 14:39 ||I| 		 Mode: 0
12.06.2019 14:39 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 14:39 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 14:39 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 14:39 ||I| Waiting for DDR3 to finish initial calibration
12.06.2019 14:39 ||I| [32mSuccessfully configured Board 0[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 0[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 1[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 2[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 3[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 4[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 5[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 6[0m
12.06.2019 14:39 ||I| [32mSuccessfully configured Cbc 7[0m
12.06.2019 14:39 ||I| Trigger source 01: 7
12.06.2019 14:39 ||I| Histo Map for Module 0 does not exist - creating 
12.06.2019 14:39 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
12.06.2019 14:39 ||I| 7
12.06.2019 14:39 ||I| Mesuring Efficiency per Strip ... 
12.06.2019 14:39 ||I| Taking data with 100 Events!
12.06.2019 14:39 ||I| 		Mean occupancy for the Top side: 64.0404 error= 15.9156[0m
12.06.2019 14:39 ||I| 		Mean occupancy for the Botton side: 65.1232 error= 15.4157[0m
# Noisy channels on Bottom Sensor : 7,8,11,17,29,31,35,42,47,48,50,52,53,57,61,63,64,73,75,77,81,82,83,89,91,95,97,99,102,103,104,106,107,108,115,121,128,130,133,158,159,163,166,172,174,179,185,188,191,192,203,204,205,209,217,219,221,224,225,230,233,236,239,240,242,245,248,249,252,256,261,264,270,271,272,273,275,276,285,288,289,290,291,297,298,299,304,309,314,315,316,317,318,323,324,332,333,344,345,348,349,350,352,353,357,365,367,368,369,371,372,375,378,381,383,385,390,399,408,409,412,414,415,418,424,436,439,447,465,466,470,476,479,480,482,487,490,497,500,503,504,505,506,508,513,514,518,520,521,529,536,537,542,549,551,553,554,563,564,565,566,568,571,572,573,577,579,587,589,595,604,605,609,611,615,617,620,621,627,628,630,631,634,635,638,639,641,642,643,645,646,650,652,654,660,663,664,665,667,670,671,674,678,679,681,684,686,687,690,691,694,702,704,705,707,708,709,710,713,716,717,721,724,725,726,727,730,731,732,737,738,739,740,744,745,746,747,750,751,752,755,756,758,759,760,762,763,764,766,767,771,772,774,778,779,781,783,787,789,791,795,796,798,804,806,807,808,812,813,817,821,823,825,826,828,829,834,835,838,839,842,847,851,854,856,864,865,872,873,874,878,879,880,883,885,887,890,891,892,893,894,895,896,897,898,899,900,901,902,903,904,905,906,907,908,909,910,911,912,913,914,915,916,917,918,919,920,921,922,923,924,925,926,927,928,929,930,931,932,933,934,935,936,937,938,939,940,941,942,943,944,945,946,947,948,949,950,951,952,953,954,955,956,957,958,959,960,961,962,963,964,965,966,967,968,969,970,971,972,973,974,975,976,977,978,979,980,981,982,983,984,985,986,987,988,989,990,991,992,993,994,995,996,997,998,999,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015
# Noisy channels on Top Sensor : 3,4,8,10,19,21,25,26,27,29,37,38,42,43,45,48,56,58,61,66,69,73,77,79,86,96,99,100,102,110,122,126,138,140,141,142,147,149,153,154,163,165,168,169,170,178,185,190,193,194,196,205,216,227,232,233,234,236,237,238,241,242,249,250,255,258,260,262,263,270,275,289,292,294,296,306,307,308,309,311,315,316,318,324,328,329,335,336,341,343,348,349,357,363,367,369,370,374,377,378,381,383,385,390,392,394,397,411,425,432,434,439,454,463,464,476,493,496,498,500,503,505,506,508,512,516,518,524,527,531,539,541,545,553,558,560,581,598,600,605,608,614,615,617,620,630,635,636,638,639,640,646,648,650,651,652,653,654,656,659,662,665,666,668,670,673,674,675,677,681,683,684,686,687,689,690,691,697,699,702,707,709,715,716,717,718,721,725,727,728,729,731,742,743,744,745,747,748,749,750,752,753,754,755,758,760,761,766,767,776,780,792,793,795,799,800,803,811,825,831,834,837,838,839,849,850,853,854,856,857,861,863,865,871,873,874,877,878,881,884,886,887,888,890,891,892,893,894,895,896,897,898,899,900,901,902,903,904,905,906,907,908,909,910,911,912,913,914,915,916,917,918,919,920,921,922,923,924,925,926,927,928,929,930,931,932,933,934,935,936,937,938,939,940,941,942,943,944,945,946,947,948,949,950,951,952,953,954,955,956,957,958,959,960,961,962,963,964,965,966,967,968,969,970,971,972,973,974,975,976,977,978,979,980,981,982,983,984,985,986,987,988,989,990,991,992,993,994,995,996,997,998,999,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015
# Dead channels on Bottom Sensor : 441
# Dead channels on Top Sensor : 
12.06.2019 14:39 ||I| Results saved!
(Noise) Occupancy measurement finished at: Wed Jun 12 14:39:42 2019
	elapsed time: 1.37658 seconds
Complete system test finished at: Wed Jun 12 14:39:42 2019
	elapsed time: 12.6983 seconds
12.06.2019 14:39 ||I| [1m[31mclosing result file![0m
12.06.2019 14:39 ||I| [1m[31mDestroying memory objects[0m
User comment: 
